package br.com.desafioandroidenjoei.base.extensions

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View

fun View?.visible() {
    this?.visibility = View.VISIBLE
}

fun View?.gone() {
    this?.visibility = View.GONE
}

fun View?.invisible() {
    this?.visibility = View.INVISIBLE
}

fun View?.isGone() = this?.visibility == View.GONE

fun RecyclerView?.setGridLayout(
    spaceCount: Int,
    hasFixedSize: Boolean = false,
    orientation: Int = GridLayoutManager.VERTICAL,
    isReverse: Boolean = false
) {
    this?.apply {
        setHasFixedSize(hasFixedSize)
        layoutManager = GridLayoutManager(this.context, spaceCount, orientation, isReverse)
    }
}

fun Activity?.contentView(): View = this!!.findViewById<View>(android.R.id.content)

fun Fragment?.contentView() = this!!.activity!!.contentView()