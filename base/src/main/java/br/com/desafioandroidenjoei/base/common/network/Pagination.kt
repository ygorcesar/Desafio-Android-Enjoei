package br.com.desafioandroidenjoei.base.common.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Pagination(
    @Json(name = "total_pages")
    var totalPages: Int = 1,
    @Json(name = "current_page")
    var currentPage: Int = 1
)