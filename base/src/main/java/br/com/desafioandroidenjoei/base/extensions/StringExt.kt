package br.com.desafioandroidenjoei.base.extensions

fun String?.fromCloudinary(crop: String = "", gravity: String = ""): String {
    val cropParam = if (crop.isNotBlank()) ",c_$crop" else ""
    val gravityParam = if (gravity.isNotBlank()) ",g_$gravity" else ""

    val cloudinaryBaseUrl =
        "http://res.cloudinary.com/demo/image/upload/w_150,h_200$cropParam$gravityParam"
    return "$cloudinaryBaseUrl/$this.jpg"
}