package br.com.desafioandroidenjoei.base.presentation

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import br.com.desafioandroidenjoei.base.common.exception.EnjoeiException
import br.com.desafioandroidenjoei.testing.OpenForTesting
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

@OpenForTesting
abstract class BaseViewModel : ViewModel() {

    val enjoeiException = MutableLiveData<EnjoeiException>()

    protected val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    protected fun handleFailure(enjoeiException: Throwable) {
        if (enjoeiException is EnjoeiException) {
            this.enjoeiException.postValue(enjoeiException)
        }
        Timber.e(enjoeiException)
    }
}