package br.com.desafioandroidenjoei.base.presentation

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.desafioandroidenjoei.base.common.exception.EnjoeiException
import br.com.desafioandroidenjoei.base.common.exception.HttpError
import br.com.desafioandroidenjoei.base.common.exception.NetworkError
import br.com.desafioandroidenjoei.base.common.exception.ServerError
import br.com.desafioandroidenjoei.base.di.base
import timber.log.Timber

abstract class BaseActivity : AppCompatActivity() {

    abstract val layoutResId: Int

    val viewModelFactory: ViewModelProvider.Factory by lazy {
        base().viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
    }

    /**
     * Check commons exception between application
     *
     * @param enjoeiException The exception returned from backend
     * @param body The function to check specific business exceptions
     */
    fun checkResponseException(
        enjoeiException: EnjoeiException?,
        body: (EnjoeiException?) -> Unit
    ) {
        Timber.e(enjoeiException)
        when (enjoeiException) {
            NetworkError -> {
                onNetworkWithoutConnection()
            }
            HttpError -> {
                onHttpError()
            }
            is ServerError -> {
                onResponseError()
            }
            else -> {
                body(enjoeiException)
            }
        }
    }

    /**
     * Function called when handled a Http generic exception
     */
    open fun onHttpError() {}

    /**
     * Function called when there is no internet connection
     */
    open fun onNetworkWithoutConnection() {}

    open fun onResponseError() {}
}