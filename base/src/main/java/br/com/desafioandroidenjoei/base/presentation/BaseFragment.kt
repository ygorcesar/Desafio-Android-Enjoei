package br.com.desafioandroidenjoei.base.presentation

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.desafioandroidenjoei.base.common.exception.EnjoeiException
import br.com.desafioandroidenjoei.base.common.exception.HttpError
import br.com.desafioandroidenjoei.base.common.exception.NetworkError
import br.com.desafioandroidenjoei.base.common.exception.ServerError
import br.com.desafioandroidenjoei.base.di.base
import timber.log.Timber

abstract class BaseFragment : Fragment() {

    abstract val layoutResId: Int

    val viewModelFactory: ViewModelProvider.Factory by lazy {
        base().viewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutResId, container, false)
    }

    /**
     * Check commons exception between application
     *
     * @param enjoeiException The exception returned from backend
     * @param body The function to check specific business exceptions
     */
    fun checkResponseException(
        enjoeiException: EnjoeiException?,
        body: (EnjoeiException?) -> Unit
    ) {
        Timber.e(enjoeiException)
        when (enjoeiException) {
            NetworkError -> {
                onNetworkWithoutConnection()
            }
            HttpError -> {
                onHttpError()
            }
            is ServerError -> {
                onResponseError()
            }
            else -> {
                body(enjoeiException)
            }
        }
    }

    /**
     * Function called when handled a Http generic exception
     */
    open fun onHttpError() {}

    /**
     * Function called when there is no internet connection
     */
    open fun onNetworkWithoutConnection() {
    }

    open fun onResponseError() {}
}