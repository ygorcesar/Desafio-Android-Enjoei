package br.com.desafioandroidenjoei.base.presentation

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import br.com.desafioandroidenjoei.base.di.base
import com.squareup.moshi.Moshi

interface FragmentArgs<T : Fragment> {

    /**
     * Create a fragment instance to launch
     * @return returns an fragment instance that can be used to launch this fragment.
     */
    fun newInstance(moshi: Moshi = base().moshi): T

    /**
     * Launches the fragment.
     *
     * The default implementation uses the fragment instance generated from [T]
     *
     * @param fragmentManager The current FragmentManager
     * @param containerId id of view container from the current fragment to be replaced
     * @param addToBackStack If true add new instance of fragment to back stack
     */
    fun launch(fragmentManager: FragmentManager, containerId: Int, addToBackStack: Boolean = true) {
        fragmentManager.beginTransaction()
            .replace(containerId, newInstance())
            .apply { if (addToBackStack) addToBackStack(null) }
            .commitAllowingStateLoss()
    }
}