package br.com.desafioandroidenjoei.base.extensions

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

inline fun <reified T> Moshi.mutableListAdapter(): JsonAdapter<MutableList<T>> =
    adapter<MutableList<T>>(Types.newParameterizedType(MutableList::class.java, T::class.java))