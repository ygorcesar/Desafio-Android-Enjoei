package br.com.desafioandroidenjoei.base.extensions

import java.text.NumberFormat
import java.util.*
import kotlin.math.roundToInt

fun Double?.toCurrency(): String {
    this?.let {
        return NumberFormat.getCurrencyInstance(Locale.getDefault()).format(it).replace(",00", "")
    }
    return ""
}

fun Double?.toCurrencyWithoutSymbol(): String {
    this?.let {
        return NumberFormat.getCurrencyInstance(Locale.getDefault()).format(it)
            .replace(",00", "")
            .replace("[R$]".toRegex(), "")
    }
    return ""
}

fun Double?.toPercentDiscount(): String {
    val percentDiscount = (this ?: 0.0).roundToInt()
    if (percentDiscount > 0) {
        return "${percentDiscount * -1}%"
    }
    return ""
}