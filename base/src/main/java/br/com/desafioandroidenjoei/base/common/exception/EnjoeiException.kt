package br.com.desafioandroidenjoei.base.common.exception

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific should extend [EnjoeiException] class.
 */
sealed class EnjoeiException(message: String = "") : RuntimeException(message)

/**
 * Object used to identify a generic http request exception
 */
object HttpError : EnjoeiException("Server http response code is not 200, 400 or 403")

/**
 * Object used to identify a network without internet connection
 */
object NetworkError : EnjoeiException()

/**
 * Object used to identify that need fetch new data
 * */
object NeedFetchData : EnjoeiException("Need fetch data from remote repository")

/**
 * Exception used when the server returns business validations
 */
object ServerError : EnjoeiException()

/**
 * Extend this class for feature specific failures.
 *
 * */
abstract class BusinessException : EnjoeiException()

/**
 * Exception thrown when an essential parameter is missing in the
 * backend/network response.
 *
 */
class EssentialParamMissingException(
    missingParam: String,
    rawObject: Any
) : EnjoeiException("The params: $missingParam are missing in received object: $rawObject")
