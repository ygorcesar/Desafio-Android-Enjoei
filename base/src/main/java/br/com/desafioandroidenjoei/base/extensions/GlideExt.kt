package br.com.desafioandroidenjoei.base.extensions

import android.support.annotation.DrawableRes
import android.widget.ImageView
import br.com.desafioandroidenjoei.base.R
import br.com.desafioandroidenjoei.base.di.GlideApp

fun ImageView?.loadImage(url: String, @DrawableRes drawableResId: Int = R.drawable.ic_image_placeholder) {
    this?.apply {
        GlideApp.with(this)
            .load(url)
            .placeholder(drawableResId)
            .into(this)
    }
}