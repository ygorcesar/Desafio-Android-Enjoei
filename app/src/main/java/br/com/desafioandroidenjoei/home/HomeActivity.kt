package br.com.desafioandroidenjoei.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.presentation.BaseActivity
import br.com.desafioandroidenjoei.camera.CameraFragment
import br.com.desafioandroidenjoei.messages.MessagesFragment
import br.com.desafioandroidenjoei.products.presentation.ProductsFragment
import br.com.desafioandroidenjoei.profile.ProfileFragment
import br.com.desafioandroidenjoei.search.SearchFragment
import kotlinx.android.synthetic.main.home_activity.*
import timber.log.Timber

class HomeActivity : BaseActivity() {

    override val layoutResId = R.layout.home_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupNavigationFragments(
            listOf(
                ProductsFragment.newInstance(),
                SearchFragment.newInstance(),
                CameraFragment.newInstance(),
                MessagesFragment.newInstance(),
                ProfileFragment.newInstance()
            )
        )
        setupBottomViewNavigation()
    }

    private fun setupBottomViewNavigation() {
        bottomNavigation?.apply {
            disableShiftMode(this)
            setOnNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.actionHome -> viewPager.setCurrentItem(POS_FRAGMENT_HOME, true)
                    R.id.actionSearch -> viewPager.setCurrentItem(POS_FRAGMENT_SEARCH, true)
                    R.id.actionCamera -> viewPager.setCurrentItem(POS_FRAGMENT_CAMERA, true)
                    R.id.actionMail -> viewPager.setCurrentItem(POS_FRAGMENT_MESSAGES, true)
                    R.id.actionUser -> viewPager.setCurrentItem(POS_FRAGMENT_PROFILE, true)
                }
                true
            }
        }
    }

    private fun setupNavigationFragments(fragments: List<Fragment>) {
        if (fragments.size in 1..5) {
            viewPager?.apply {
                adapter = HomePagerAdapter(supportFragmentManager, fragments)
                offscreenPageLimit = fragments.size
                addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(state: Int) {}
                    override fun onPageScrolled(
                        position: Int,
                        positionOffset: Float,
                        positionOffsetPixels: Int
                    ) {
                    }

                    override fun onPageSelected(position: Int) {
                        bottomNavigation?.apply {
                            selectedItemId = when (position) {
                                POS_FRAGMENT_SEARCH -> R.id.actionSearch
                                POS_FRAGMENT_CAMERA -> R.id.actionCamera
                                POS_FRAGMENT_MESSAGES -> R.id.actionMail
                                POS_FRAGMENT_PROFILE -> R.id.actionUser
                                else -> R.id.actionHome
                            }
                        }
                    }
                })
            }
        } else {
            throw RuntimeException("Need a list of fragments with one or max of five Fragments!")
        }
    }

    /**
     * Helper to disable shift animation from NavigatioView
     */
    @SuppressLint("RestrictedApi")
    private fun disableShiftMode(view: BottomNavigationView) {
        val menuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView::class.java.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShiftingMode(false)
                item.setChecked(item.itemData.isChecked)
            }
        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    companion object {
        const val POS_FRAGMENT_HOME = 0
        const val POS_FRAGMENT_SEARCH = 1
        const val POS_FRAGMENT_CAMERA = 2
        const val POS_FRAGMENT_MESSAGES = 3
        const val POS_FRAGMENT_PROFILE = 4
    }
}