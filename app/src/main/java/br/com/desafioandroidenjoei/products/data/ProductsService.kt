package br.com.desafioandroidenjoei.products.data

import br.com.desafioandroidenjoei.products.model.ProductsResponseRaw
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductsService {

    @GET("products/home")
    fun getProducts(@Query("page") page: Int = 1): Single<ProductsResponseRaw>
}