package br.com.desafioandroidenjoei.products.model

import android.arch.persistence.room.ColumnInfo

data class Avatar(
    @ColumnInfo(name = "avatar_gravity") var gravity: String = "",
    @ColumnInfo(name = "avatar_crop") var crop: String = "",
    @ColumnInfo(name = "avatar_public_id") var publicId: String = ""
)