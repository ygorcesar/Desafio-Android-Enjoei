package br.com.desafioandroidenjoei.products.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotosProductRaw(
    @Json(name = "gravity") var gravity: String?,
    @Json(name = "crop") var crop: String?,
    @Json(name = "public_id") var publicId: String?
)