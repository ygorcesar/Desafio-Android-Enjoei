package br.com.desafioandroidenjoei.products.viewmodel

import android.arch.lifecycle.MutableLiveData
import br.com.desafioandroidenjoei.base.data.ViewState
import br.com.desafioandroidenjoei.base.extensions.addToComposite
import br.com.desafioandroidenjoei.base.presentation.BaseViewModel
import br.com.desafioandroidenjoei.products.domain.ProductsInteractor
import br.com.desafioandroidenjoei.products.model.ProductsResponse
import br.com.desafioandroidenjoei.testing.OpenForTesting
import com.squareup.moshi.Moshi
import javax.inject.Inject

@OpenForTesting
class ProductsViewModel
@Inject constructor(
    private val productsInteractor: ProductsInteractor,
    val moshi: Moshi
) : BaseViewModel() {

    private var page: Int = 1
    var totalPages: Int = 1
    var productsResponse: ProductsResponse? = null
    val productsState = MutableLiveData<ViewState>()

    fun getProducts(fetchFromRemote: Boolean = true) {
        compositeDisposable.clear()

        productsInteractor.getProducts(page, fetchFromRemote)
            .doOnSubscribe { productsState.postValue(ViewState.Loading) }
            .subscribe({ productResponse ->
                page++
                productsState.postValue(ViewState.Complete(productResponse))
            }, {
                handleFailure(it)
            })
            .addToComposite(compositeDisposable)
    }

    fun setTotalPage(totalPages: Int?) {
        totalPages?.let { this.totalPages = it }
    }

    fun invalidatePages() {
        page = 1
        totalPages = 1
    }

    /**
     * Using fixed totalPage 4 only to load more pages, because on API limited to 2 pages
     */
    fun isLastPage() = page > /*totalPages*/4

    fun addProducts(productResponse: ProductsResponse) {
        if (this.productsResponse == null) {
            this.productsResponse = productResponse
        } else {
            this.productsResponse?.products?.addAll(productResponse.products)
        }
    }
}