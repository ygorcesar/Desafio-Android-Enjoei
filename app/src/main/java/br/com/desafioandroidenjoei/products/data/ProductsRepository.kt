package br.com.desafioandroidenjoei.products.data

import br.com.desafioandroidenjoei.base.common.network.NetworkHandler
import br.com.desafioandroidenjoei.base.data.BaseRemoteRepository
import br.com.desafioandroidenjoei.base.extensions.performOnBack
import br.com.desafioandroidenjoei.products.model.ProductsResponse
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class ProductsRepository
@Inject constructor(private val local: Local, private val remote: Remote) {

    fun getProducts(page: Int = 1, fetchFromRemote: Boolean = true): Single<ProductsResponse> {
        return when (fetchFromRemote) {
            true -> remote.getProducts(page).doOnSuccess { local.saveProducts(it) }
            false -> local.getProducts(page)
        }
    }

    interface ProductsContract {
        fun getProducts(page: Int = 1): Single<ProductsResponse>
    }

    interface ProductsContractLocal : ProductsContract {
        fun saveProducts(products: ProductsResponse)
    }

    interface ProductsContractRemote : ProductsContract

    class Local
    @Inject constructor(
        private val productsDao: ProductsDao
    ) : ProductsContractLocal {

        override fun getProducts(page: Int): Single<ProductsResponse> {
            return Single.create<ProductsResponse> {
                it.onSuccess(ProductsResponse(productsDao.getProducts().toMutableList()))
            }.performOnBack().doOnSuccess { Timber.i("Products fetched from local") }
        }

        override fun saveProducts(products: ProductsResponse) {
            val compositeDisposable = CompositeDisposable()
            val updateProducts = Completable.create {
                productsDao.insertProducts(products.products)
                it.onComplete()
            }.performOnBack()
                .doOnComplete { compositeDisposable.clear() }
                .subscribe({ Timber.i("Products updated") }, { it.printStackTrace() })

            compositeDisposable.add(updateProducts)
        }
    }

    class Remote
    @Inject constructor(
        private val productsService: ProductsService,
        private val productsResponseMapper: ProductsResponseMapper,
        networkHandler: NetworkHandler
    ) : BaseRemoteRepository(networkHandler), ProductsContractRemote {

        override fun getProducts(page: Int): Single<ProductsResponse> =
            request(productsResponseMapper) {
                productsService.getProducts(page).performOnBack()
            }
    }
}