package br.com.desafioandroidenjoei.products.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserRaw(
    @Json(name = "name") var name: String?,
    @Json(name = "id") var id: Int?,
    @Json(name = "avatar") var avatarRaw: AvatarRaw?
)