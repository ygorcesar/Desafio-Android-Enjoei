package br.com.desafioandroidenjoei.products.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductRaw(
    @Json(name = "id") var id: Int?,
    @Json(name = "discount_percentage") var discountPercentage: Double?,
    @Json(name = "photos") var photos: List<PhotosProductRaw>?,
    @Json(name = "title") var title: String?,
    @Json(name = "price") val price: Double?,
    @Json(name = "original_price") var originalPrice: Double?,
    @Json(name = "size") var size: String?,
    @Json(name = "likes_count") var likesCount: Int?,
    @Json(name = "maximum_installment") var maximumInstallment: Int?,
    @Json(name = "published_comments_count") var publishedCommentsCount: Int?,
    @Json(name = "content") var content: String?,
    @Json(name = "user") var userRaw: UserRaw?
)