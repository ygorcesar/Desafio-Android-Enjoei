package br.com.desafioandroidenjoei.products.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "product")
data class Product(
    @PrimaryKey var id: Int = 0,
    var discountPercentage: Double = 0.0,
    var photos: List<PhotosProduct> = listOf(),
    var title: String = "",
    var price: Double = 0.0,
    var originalPrice: Double = 0.0,
    var size: String = "",
    var likesCount: Int = 0,
    var maximumInstallment: Int = 0,
    var publishedCommentsCount: Int = 0,
    var content: String = "",
    @Embedded var user: User = User(),
    var responseId: Int? = null
)