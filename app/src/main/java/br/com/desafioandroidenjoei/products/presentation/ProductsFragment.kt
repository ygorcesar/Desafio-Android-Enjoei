package br.com.desafioandroidenjoei.products.presentation

import android.os.Bundle
import android.support.v4.util.Pair
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.common.exception.EnjoeiException
import br.com.desafioandroidenjoei.base.data.ViewState
import br.com.desafioandroidenjoei.base.extensions.*
import br.com.desafioandroidenjoei.base.presentation.BaseFragment
import br.com.desafioandroidenjoei.base.presentation.BaseRecyclerViewAdapter
import br.com.desafioandroidenjoei.core.extensions.snack
import br.com.desafioandroidenjoei.product_detail.presentation.ProductDetailActivityArgs
import br.com.desafioandroidenjoei.products.domain.ProductsBusiness
import br.com.desafioandroidenjoei.products.model.Product
import br.com.desafioandroidenjoei.products.model.ProductsResponse
import br.com.desafioandroidenjoei.products.viewmodel.ProductsViewModel
import br.com.desafioandroidenjoei.testing.OpenForTesting
import kotlinx.android.synthetic.main.default_progress_bar.*
import kotlinx.android.synthetic.main.no_content_view.*
import kotlinx.android.synthetic.main.product_item.view.*
import kotlinx.android.synthetic.main.products_fragment.*
import timber.log.Timber

@OpenForTesting
class ProductsFragment : BaseFragment() {

    override val layoutResId = R.layout.products_fragment
    private var refreshingBySwipe = false
    private var withoutConnection = false

    private lateinit var viewModel: ProductsViewModel
    private val adapter: BaseRecyclerViewAdapter<Product> by lazy {
        BaseRecyclerViewAdapter(layoutResId = R.layout.product_item, bindView = ::bindView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupSwipeToRefresh()
        setupTryAgain()

        viewModel = provideViewModel(viewModelFactory) {
            if (productsResponse == null) getProducts()
            else addItems(productsResponse!!, updateViewModel = false)

            observe(productsState, ::onResponseStateChanged)
            failure(enjoeiException, ::onResponseFailure)
        }
    }

    private fun onResponseStateChanged(viewState: ViewState?) {
        when (viewState) {
            ViewState.Loading -> {
                if (!refreshingBySwipe) loading(true)
            }
            is ViewState.Complete<*> -> {
                if (productsRecyclerView.isGone()) {
                    productsRecyclerView.visible()
                    noConnectionContent.gone()
                }

                banner.visible()
                invalidateItems()
                loading(false)
                addItems(viewState.response as ProductsResponse)
            }
        }
    }

    private fun onResponseFailure(enjoeiException: EnjoeiException?) {
        checkResponseException(enjoeiException) {
            when (it) {
                ProductsBusiness.InvalidPage -> Timber.e("Invalid page!")
            }
        }
    }

    private fun setupAdapter() {
        productsRecyclerView?.apply {
            setGridLayout(spaceCount = 2, hasFixedSize = true)
            adapter = this@ProductsFragment.adapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val gridLayoutManager = layoutManager as GridLayoutManager
                    val visibleItemCount = gridLayoutManager.childCount
                    val totalItemCount = gridLayoutManager.itemCount
                    val firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition()

                    if (viewModel.productsState.value !== ViewState.Loading && !viewModel.isLastPage()) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount &&
                            firstVisibleItemPosition >= 0 &&
                            !withoutConnection
                        ) {
                            viewModel.getProducts()
                        }
                    }
                }
            })
        }
    }

    private fun setupSwipeToRefresh() {
        productsSwipeToRefresh?.apply {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener {
                refreshingBySwipe = true
                withoutConnection = false
                viewModel.invalidatePages()
                viewModel.getProducts()
            }
        }
    }

    private fun addItems(productsResponse: ProductsResponse, updateViewModel: Boolean = true) {
        if (updateViewModel) {
            viewModel.apply {
                addProducts(productsResponse)
                setTotalPage(productsResponse.pagination.totalPages)
            }
        }
        productsResponse.products.forEach { this@ProductsFragment.adapter.addItem(it) }
    }

    private fun bindView(view: View, item: Product) {
        view.apply {
            setOnClickListener { navigateToProductDetail(this, item) }
            val productPhotoUrl = item.photos[0].run { publicId.fromCloudinary(crop, gravity) }
            val userPhotoUrl = item.user.avatar.run { publicId.fromCloudinary(crop, gravity) }

            productImage.loadImage(productPhotoUrl)
            productUserPhoto.loadImage(userPhotoUrl)
            productName.text = item.title
            productPrice.text = item.price.toCurrency()
            productLikeCount.text = if (item.likesCount > 0) item.likesCount.toString() else ""

            val size = if (item.size.isBlank()) "" else getString(R.string.product_size, item.size)
            productSize.text = size

            item.discountPercentage.toPercentDiscount().let { percentDiscount ->
                if (percentDiscount.isBlank()) {
                    productDiscount.gone()
                } else {
                    productDiscount.apply {
                        text = percentDiscount
                        visible()
                    }
                }
            }
        }
    }

    fun navigateToProductDetail(itemView: View, product: Product) {
        itemView.apply {
            ProductDetailActivityArgs(product).launch(
                activity!!,
                withTransition = true,
                elements = *arrayOf(
                    Pair.create(productImage as View, getString(R.string.transition_product_image)),
                    Pair.create(productName as View, getString(R.string.transition_product_name)),
                    Pair.create(productPrice as View, getString(R.string.transition_product_price))
                )
            )
        }
    }

    private fun loading(isLoading: Boolean) {
        productsSwipeToRefresh?.isRefreshing = false
        val visibility = if (isLoading) View.VISIBLE else View.GONE
        progressBar?.visibility = visibility
    }

    private fun invalidateItems() {
        if (refreshingBySwipe) {
            adapter.clearItems()
            refreshingBySwipe = false
        }
    }

    private fun showEmptyViewNoConnection() {
        if (!withoutConnection) {
            loading(false)
            banner.gone()
            productsRecyclerView.gone()
            noConnectionContent.visible()
        }
    }

    private fun setupTryAgain() {
        noConnectionTryAgain.setOnClickListener {
            val fetchFromRemote = if (withoutConnection) {
                snack(R.string.fetching_from_cache)
                false
            } else {
                true
            }
            viewModel.getProducts(fetchFromRemote)
        }
    }

    override fun onNetworkWithoutConnection() {
        loading(false)
        super.onNetworkWithoutConnection()
        showEmptyViewNoConnection()
        withoutConnection = true
    }

    override fun onHttpError() {
        loading(false)
        super.onHttpError()
        showEmptyViewNoConnection()
    }

    override fun onResponseError() {
        super.onResponseError()
        snack(R.string.server_error)
        showEmptyViewNoConnection()
    }

    companion object {
        fun newInstance() = ProductsFragment()
    }
}