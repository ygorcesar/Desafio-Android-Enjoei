package br.com.desafioandroidenjoei.products.domain

import br.com.desafioandroidenjoei.products.data.ProductsRepository
import br.com.desafioandroidenjoei.products.model.ProductsResponse
import io.reactivex.Single
import javax.inject.Inject

class ProductsInteractor
@Inject constructor(private val productsRepository: ProductsRepository) {

    fun getProducts(page: Int, fetchFromRemote: Boolean): Single<ProductsResponse> {
        return when {
            page < 1 -> Single.error(ProductsBusiness.InvalidPage)
            else -> productsRepository.getProducts(page, fetchFromRemote)
        }
    }
}