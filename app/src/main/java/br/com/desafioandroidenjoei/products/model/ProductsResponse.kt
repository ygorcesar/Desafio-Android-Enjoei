package br.com.desafioandroidenjoei.products.model

import br.com.desafioandroidenjoei.base.common.network.Pagination

data class ProductsResponse(
    var products: MutableList<Product> = mutableListOf(),
    var pagination: Pagination = Pagination()
)