package br.com.desafioandroidenjoei.products.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    @ColumnInfo(name = "user_name") var name: String = "",
    @ColumnInfo(name = "user_id") var id: Int = 0,
    @Embedded var avatar: Avatar = Avatar()
)