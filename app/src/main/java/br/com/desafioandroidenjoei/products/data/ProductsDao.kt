package br.com.desafioandroidenjoei.products.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.com.desafioandroidenjoei.products.model.Product

@Dao
interface ProductsDao {

    @Query("SELECT * FROM product")
    fun getProducts(): List<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducts(products: MutableList<Product>)
}