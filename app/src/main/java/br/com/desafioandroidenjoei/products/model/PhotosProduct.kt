package br.com.desafioandroidenjoei.products.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "photos_product")
data class PhotosProduct(
    @ColumnInfo(name = "photo_gravity") var gravity: String = "",
    @ColumnInfo(name = "photo_crop") var crop: String = "",
    @ColumnInfo(name = "photo_public_id") var publicId: String = "",
    @PrimaryKey(autoGenerate = true) var photoProductId: Int? = null,
    var productPhotoId: Int = 0
)