package br.com.desafioandroidenjoei.products.domain

import br.com.desafioandroidenjoei.base.common.exception.BusinessException

sealed class ProductsBusiness {

    object InvalidPage : BusinessException()
}