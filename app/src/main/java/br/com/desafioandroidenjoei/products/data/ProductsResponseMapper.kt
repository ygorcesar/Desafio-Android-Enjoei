package br.com.desafioandroidenjoei.products.data

import br.com.desafioandroidenjoei.base.common.exception.EssentialParamMissingException
import br.com.desafioandroidenjoei.base.common.network.Pagination
import br.com.desafioandroidenjoei.base.data.BaseMapper
import br.com.desafioandroidenjoei.products.model.*
import javax.inject.Inject

class ProductsResponseMapper
@Inject constructor() : BaseMapper<ProductsResponseRaw, ProductsResponse>() {

    override fun assertEssentialParams(raw: ProductsResponseRaw) {
        if (raw.products == null) addMissingParam("products")

        raw.products?.forEach {
            if (it.id == null) addMissingParam("id")

            if (it.discountPercentage == null) addMissingParam("discountPercentege")

            if (it.photos == null) addMissingParam("photos")

            if (it.title == null) addMissingParam("title")

            if (it.price == null) addMissingParam("price")

            if (it.originalPrice == null) addMissingParam("originalPrice")

            if (it.content == null) addMissingParam("content")

            if (it.userRaw == null) addMissingParam("user")

            it.userRaw?.let { userRaw ->
                if (userRaw.id == null) addMissingParam("userId")

                if (userRaw.name == null) addMissingParam("userName")
            }
        }

        if (isMissingParams()) throw EssentialParamMissingException(getMissingParams(), raw)
    }

    override fun copyValues(raw: ProductsResponseRaw): ProductsResponse {
        val pagination = Pagination(
            raw.pagination?.totalPages ?: 1,
            raw.pagination?.currentPage ?: 1
        )

        val products = kotlin.run {
            val products = mutableListOf<Product>()
            raw.products!!.forEach { prods ->
                val product = prods.run {

                    val productPhotos = photos.run {
                        val photos = mutableListOf<PhotosProduct>()
                        this?.forEach { photo ->
                            photos.add(
                                PhotosProduct(
                                    photo.gravity ?: "",
                                    photo.crop ?: "",
                                    photo.publicId ?: ""
                                )
                            )
                        }
                        photos
                    }

                    val user = userRaw.run {
                        val avatar = this!!.avatarRaw.run {
                            Avatar(
                                this?.gravity ?: "",
                                this?.crop ?: "",
                                this?.publicId ?: ""
                            )
                        }

                        User(name!!, id!!, avatar)
                    }

                    Product(
                        id!!,
                        discountPercentage!!,
                        productPhotos,
                        title!!,
                        price!!,
                        originalPrice!!,
                        size ?: "",
                        likesCount ?: 0,
                        maximumInstallment ?: 0,
                        publishedCommentsCount ?: 0,
                        content!!,
                        user
                    )
                }
                products.add(product)
            }
            products
        }
        return ProductsResponse(products, pagination)
    }
}