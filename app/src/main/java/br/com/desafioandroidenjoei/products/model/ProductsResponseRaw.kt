package br.com.desafioandroidenjoei.products.model

import br.com.desafioandroidenjoei.base.common.network.Pagination
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductsResponseRaw(
    @Json(name = "products") var products: List<ProductRaw>?,
    @Json(name = "pagination") var pagination: Pagination?
)