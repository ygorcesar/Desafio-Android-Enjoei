package br.com.desafioandroidenjoei.camera

import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.presentation.BaseFragment

class CameraFragment : BaseFragment() {

    override val layoutResId = R.layout.camera_fragment

    companion object {
        fun newInstance() = CameraFragment()
    }
}