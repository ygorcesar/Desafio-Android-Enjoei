package br.com.desafioandroidenjoei.core.room

import android.arch.persistence.room.TypeConverter
import br.com.desafioandroidenjoei.base.extensions.mutableListAdapter
import br.com.desafioandroidenjoei.core.di.app
import br.com.desafioandroidenjoei.products.model.PhotosProduct

class Converters {

    @TypeConverter
    fun toPhotosProducts(value: String): MutableList<PhotosProduct> {
        return app().moshi.mutableListAdapter<PhotosProduct>().fromJson(value) ?: mutableListOf()
    }

    @TypeConverter
    fun fromPhotosProducts(products: MutableList<PhotosProduct>): String {
        return app().moshi.mutableListAdapter<PhotosProduct>().toJson(products)
    }
}