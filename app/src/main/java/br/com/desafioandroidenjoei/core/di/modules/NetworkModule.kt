package br.com.desafioandroidenjoei.core.di.modules

import br.com.desafioandroidenjoei.BuildConfig
import br.com.desafioandroidenjoei.base.common.exception.HttpError
import br.com.desafioandroidenjoei.base.common.exception.ServerError
import br.com.desafioandroidenjoei.core.di.scopes.ApplicationScope
import br.com.desafioandroidenjoei.products.data.ProductsService
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @ApplicationScope
    fun provideRetrofit(
        httpClient: OkHttpClient,
        baseUrl: String,
        converter: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(converter)
            .client(httpClient)
            .build()

    }

    @Provides
    @ApplicationScope
    fun provideNetworkTimeout(): Long = 120L

    @Provides
    @ApplicationScope
    fun provideBaseUrl(): String = BuildConfig.API_URL

    @Provides
    @ApplicationScope
    fun provideConverter(moshi: Moshi): Converter.Factory = MoshiConverterFactory.create(moshi)

    @Provides
    @ApplicationScope
    fun provideLogger(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Timber.d(message); })
    }

    @Provides
    @ApplicationScope
    fun provideProductsService(retrofit: Retrofit): ProductsService =
        retrofit.create(ProductsService::class.java)

    @Provides
    @ApplicationScope
    fun provideClient(
        networkTimeoutSecond: Long,
        logger: HttpLoggingInterceptor,
        retrofitInterceptor: RetrofitInterceptor
    ): OkHttpClient {

        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.readTimeout(networkTimeoutSecond, TimeUnit.SECONDS)
        okHttpClientBuilder.connectTimeout(networkTimeoutSecond, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            logger.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(logger)
        }

        okHttpClientBuilder.addInterceptor(retrofitInterceptor)

        return okHttpClientBuilder.build()
    }

    @Singleton
    class RetrofitInterceptor @Inject constructor() : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            val response = chain.proceed(request)

            when (response.code()) {
                in 200..206 -> {
                }
                400 -> throw ServerError
                else -> {
                    Timber.e("RESPONSE_CODE:${response.code()}; BODY: ${response.body()?.string().toString()} ")
                    throw HttpError
                }
            }
            return response
        }
    }
}