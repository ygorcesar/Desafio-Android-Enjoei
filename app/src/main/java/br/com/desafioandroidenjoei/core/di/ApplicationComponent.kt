package br.com.desafioandroidenjoei.core.di

import br.com.desafioandroidenjoei.base.di.BaseComponent
import br.com.desafioandroidenjoei.base.di.BaseModule
import br.com.desafioandroidenjoei.core.di.modules.ApplicationModule
import br.com.desafioandroidenjoei.core.di.modules.NetworkModule
import br.com.desafioandroidenjoei.core.di.modules.RoomModule
import br.com.desafioandroidenjoei.core.di.modules.ViewModelModule
import br.com.desafioandroidenjoei.core.di.scopes.ApplicationScope
import dagger.Component
import javax.inject.Singleton

/**
 * Function to get the current [ApplicationComponent] instance
 */
fun app() = ApplicationComponent.INSTANCE

@ApplicationScope
@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        BaseModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        RoomModule::class
    ]
)
interface ApplicationComponent : BaseComponent {

    companion object {
        lateinit var INSTANCE: ApplicationComponent
    }
}
