package br.com.desafioandroidenjoei.core.extensions

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import br.com.desafioandroidenjoei.base.extensions.contentView

fun Fragment.snack(@StringRes stringResId: Int, duration: Int = Snackbar.LENGTH_LONG) =
    Snackbar.make(contentView(), stringResId, duration).show()