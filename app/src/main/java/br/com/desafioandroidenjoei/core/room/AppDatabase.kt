package br.com.desafioandroidenjoei.core.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import br.com.desafioandroidenjoei.products.data.ProductsDao
import br.com.desafioandroidenjoei.products.model.PhotosProduct
import br.com.desafioandroidenjoei.products.model.Product

@Database(entities = [Product::class, PhotosProduct::class], version = 1)
@TypeConverters(value = [Converters::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun productsResponseDao(): ProductsDao
}