package br.com.desafioandroidenjoei.core.di.modules

import android.arch.lifecycle.ViewModel
import br.com.desafioandroidenjoei.base.di.ViewModelKey
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import br.com.desafioandroidenjoei.products.viewmodel.ProductsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProductsViewModel::class)
    abstract fun provideProductsViewModel(loginViewModel: ProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailsViewModel::class)
    abstract fun provideProductDetailsViewModel(loginViewModel: ProductDetailsViewModel): ViewModel

}