package br.com.desafioandroidenjoei.core

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import br.com.desafioandroidenjoei.BuildConfig
import br.com.desafioandroidenjoei.base.di.BaseComponent
import br.com.desafioandroidenjoei.core.di.ApplicationComponent
import br.com.desafioandroidenjoei.core.di.DaggerApplicationComponent
import br.com.desafioandroidenjoei.core.di.modules.ApplicationModule
import timber.log.Timber

open class EnjoeiApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setupTimber()

        val appComponent = createApplicationComponent()
        ApplicationComponent.INSTANCE = appComponent
        BaseComponent.INSTANCE = appComponent
    }

    open fun createApplicationComponent(): ApplicationComponent {
        return DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this@EnjoeiApplication))
            .build()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}