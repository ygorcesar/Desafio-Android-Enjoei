package br.com.desafioandroidenjoei.core.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import br.com.desafioandroidenjoei.core.di.scopes.ApplicationScope
import br.com.desafioandroidenjoei.core.room.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @ApplicationScope
    fun provideAppDatabase(context: Context) =
        Room.databaseBuilder(
            context, AppDatabase::class.java,
            "Enjoei.db"
        ).build()

    @Provides
    @Singleton
    fun provideProductsResponseDao(appDatabase: AppDatabase) = appDatabase.productsResponseDao()
}