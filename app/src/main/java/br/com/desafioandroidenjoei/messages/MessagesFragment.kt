package br.com.desafioandroidenjoei.messages

import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.presentation.BaseFragment

class MessagesFragment : BaseFragment() {

    override val layoutResId = R.layout.messages_fragment

    companion object {
        fun newInstance() = MessagesFragment()
    }
}