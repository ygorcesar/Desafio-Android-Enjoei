package br.com.desafioandroidenjoei.product_detail.presentation

import android.os.Bundle
import android.view.Menu
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.extensions.*
import br.com.desafioandroidenjoei.base.presentation.BaseActivity
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import br.com.desafioandroidenjoei.products.model.Product
import kotlinx.android.synthetic.main.product_detail_activity.*

class ProductDetailActivity : BaseActivity() {

    override val layoutResId = R.layout.product_detail_activity

    lateinit var viewModel: ProductDetailsViewModel

    private val args: ProductDetailActivityArgs by lazy {
        ProductDetailActivityArgs.deserializeFrom(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel(viewModelFactory) {
            observe(productData, ::bindView)
        }

        productDetailToolbar?.apply {
            setSupportActionBar(this)
            setNavigationOnClickListener { finishAfterTransition() }
        }

        if (viewModel.productData.value == null)
            viewModel.productData.postValue(args.product)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_product_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun bindView(product: Product?) {
        product?.apply {
            productDetailViewPager?.apply {
                adapter = ProductDetailPagerAdapter(photos, supportFragmentManager)
            }

            productDetailName.text = title
            productDetailDescription.text = content
            productDetailPrice.text = price.toCurrencyWithoutSymbol()
            productDetailCommentsCount.text = publishedCommentsCount.toString()
            productDetailPercentAndInfo.text = getString(
                R.string.product_detail_percent_and_info,
                discountPercentage.toPercentDiscount()
            )

            if (originalPrice <= 0.0) {
                productDetailOriginalPrice.gone()
            } else {
                productDetailOriginalPrice.text = originalPrice.toCurrency()
            }
        }
    }
}