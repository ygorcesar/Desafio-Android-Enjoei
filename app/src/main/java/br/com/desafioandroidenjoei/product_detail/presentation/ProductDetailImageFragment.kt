package br.com.desafioandroidenjoei.product_detail.presentation

import android.os.Bundle
import android.view.View
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.extensions.fromCloudinary
import br.com.desafioandroidenjoei.base.extensions.loadImage
import br.com.desafioandroidenjoei.base.extensions.provideViewModel
import br.com.desafioandroidenjoei.base.presentation.BaseFragment
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import kotlinx.android.synthetic.main.product_detail_image_fragment.*

class ProductDetailImageFragment : BaseFragment() {

    lateinit var viewModel: ProductDetailsViewModel

    private val args: ProductDetailImageFragmentArgs by lazy {
        ProductDetailImageFragmentArgs.deserializeFrom(this)
    }

    override val layoutResId = R.layout.product_detail_image_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = provideViewModel(viewModelFactory) {}

        args.photosProduct?.apply {
            productDetailImage.loadImage(publicId.fromCloudinary(crop, gravity))
        }
    }
}