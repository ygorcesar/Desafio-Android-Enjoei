package br.com.desafioandroidenjoei.product_detail.presentation

import android.os.Bundle
import android.support.v4.app.Fragment
import br.com.desafioandroidenjoei.base.presentation.FragmentArgs
import br.com.desafioandroidenjoei.core.di.app
import br.com.desafioandroidenjoei.products.model.PhotosProduct
import com.squareup.moshi.Moshi

class ProductDetailImageFragmentArgs(val photosProduct: PhotosProduct?) :
    FragmentArgs<ProductDetailImageFragment> {

    override fun newInstance(moshi: Moshi) = ProductDetailImageFragment().apply {
        arguments = Bundle().apply {
            val photoJson = moshi.adapter(PhotosProduct::class.java)?.toJson(photosProduct)
            putString(PRODUCT_PHOTO_KEY, photoJson)
        }
    }

    companion object {

        fun deserializeFrom(fragment: Fragment) = fragment.arguments!!.run {
            ProductDetailImageFragmentArgs(
                app().moshi.adapter(PhotosProduct::class.java).fromJson(
                    this.getString(
                        PRODUCT_PHOTO_KEY
                    )
                )
            )
        }

        const val PRODUCT_PHOTO_KEY = "product_photo_key"
    }
}