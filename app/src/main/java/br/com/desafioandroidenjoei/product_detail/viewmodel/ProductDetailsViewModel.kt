package br.com.desafioandroidenjoei.product_detail.viewmodel

import android.arch.lifecycle.MutableLiveData
import br.com.desafioandroidenjoei.base.presentation.BaseViewModel
import br.com.desafioandroidenjoei.products.model.Product
import javax.inject.Inject

class ProductDetailsViewModel
@Inject constructor() : BaseViewModel() {

    var productData = MutableLiveData<Product>()

}