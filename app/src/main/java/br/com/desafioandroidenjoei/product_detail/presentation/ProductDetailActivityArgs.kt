package br.com.desafioandroidenjoei.product_detail.presentation

import android.content.Context
import android.content.Intent
import br.com.desafioandroidenjoei.base.presentation.ActivityArgs
import br.com.desafioandroidenjoei.core.di.app
import br.com.desafioandroidenjoei.products.model.Product
import br.com.desafioandroidenjoei.testing.OpenForTesting
import com.squareup.moshi.Moshi

@OpenForTesting
class ProductDetailActivityArgs(val product: Product?) : ActivityArgs {

    override fun intent(context: Context, moshi: Moshi) = Intent(
        context, ProductDetailActivity::class.java
    ).apply {
        val productJson = moshi.adapter(Product::class.java)?.toJson(product)
        putExtra(PRODUCT_KEY, productJson)
    }

    companion object {
        fun deserializeFrom(intent: Intent) =
            ProductDetailActivityArgs(
                app().moshi.adapter(Product::class.java).fromJson(intent.getStringExtra(PRODUCT_KEY))
            )

        const val PRODUCT_KEY = "product_key"
    }
}