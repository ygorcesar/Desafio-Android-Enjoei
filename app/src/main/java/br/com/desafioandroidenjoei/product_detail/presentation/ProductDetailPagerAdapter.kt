package br.com.desafioandroidenjoei.product_detail.presentation

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import br.com.desafioandroidenjoei.products.model.PhotosProduct

class ProductDetailPagerAdapter(
    private val photos: List<PhotosProduct>,
    fm: FragmentManager
) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int) =
        ProductDetailImageFragmentArgs(photos[position]).newInstance()

    override fun getCount() = photos.size
}