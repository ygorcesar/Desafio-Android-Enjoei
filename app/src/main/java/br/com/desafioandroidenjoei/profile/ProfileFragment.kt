package br.com.desafioandroidenjoei.profile

import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.presentation.BaseFragment

class ProfileFragment : BaseFragment() {

    override val layoutResId = R.layout.profile_fragment

    companion object {
        fun newInstance() = ProfileFragment()
    }
}