package br.com.desafioandroidenjoei.search

import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.presentation.BaseFragment

class SearchFragment : BaseFragment() {

    override val layoutResId = R.layout.search_fragment

    companion object {
        fun newInstance() = SearchFragment()
    }
}