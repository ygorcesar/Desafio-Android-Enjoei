package br.com.desafioandroidenjoei.products

import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.LargeTest
import android.view.View
import android.widget.ProgressBar
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.BaseEspressoTest
import br.com.desafioandroidenjoei.base.BaseTestRobot
import br.com.desafioandroidenjoei.base.common.exception.NetworkError
import br.com.desafioandroidenjoei.base.data.ViewState
import br.com.desafioandroidenjoei.home.HomeActivity
import br.com.desafioandroidenjoei.product_detail.presentation.ProductDetailActivity
import br.com.desafioandroidenjoei.product_detail.presentation.ProductDetailActivityArgs
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import br.com.desafioandroidenjoei.products.presentation.ProductsFragment
import br.com.desafioandroidenjoei.products.viewmodel.ProductsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

@LargeTest
class ProductsTest : BaseEspressoTest() {

    @Rule
    @JvmField
    val activityTestRule = IntentsTestRule(HomeActivity::class.java, false, false)

    private val viewModel: ProductsViewModel = mockProductsViewModel()

    private val productDetailsViewModel: ProductDetailsViewModel = mockProductDetailsViewModel()

    private lateinit var robot: BaseTestRobot

    @Mock
    private lateinit var productsArgs: ProductDetailActivityArgs

    @Before
    fun setup() {

        activityTestRule.launchActivity(null)

        robot = BaseTestRobot()

        val products = productResponseMock.products[0]

        productsArgs = ProductDetailActivityArgs(products)

        val productsFragment = Mockito.mock(ProductsFragment::class.java)

        val view = activityTestRule.activity.findViewById<View>(android.R.id.content)
        Mockito.`when`(productsFragment.navigateToProductDetail(view, products)).then {
            productsArgs.launch(activityTestRule.activity)
        }
    }

    @Test
    fun whenFragmentIsLaunched_shouldDisplayInitialState() {
        robot.isDisplayed(R.id.productsRecyclerView)
        robot.isDisplayed(R.id.productsSwipeToRefresh)
        robot.isNotDisplayed(R.id.banner)
        robot.isNotDisplayed(R.id.noConnectionContent)
        robot.isNotDisplayed(R.id.progressBar)
    }

    @Test
    fun whenIsFetchingData_shouldDisplayLoading() {
        activityTestRule.runOnUiThread {
            viewModel.productsState.value = ViewState.Loading
        }
        val progressBar = activityTestRule.activity.findViewById<ProgressBar>(R.id.progressBar)
        progressBar.indeterminateDrawable = app.getDrawable(R.drawable.ic_image_placeholder)

        robot.isDisplayed(R.id.progressBar)
    }

    @Test
    fun whenDataFetched_shouldDisplayRecyclerViewWithItems() {
        activityTestRule.runOnUiThread {
            viewModel.productsState.value = ViewState.Complete(productResponseMock)
        }
        robot.isDisplayed(R.id.banner)
        robot.isDisplayed(R.id.productsRecyclerView)
        robot.isNotDisplayed(R.id.noConnectionContent)
        robot.clickRecyclerViewItem(R.id.productsRecyclerView, 0)
    }

    @Test
    fun whenItemClicked_shouldNavigateToProductDetails() {
        activityTestRule.runOnUiThread {
            viewModel.productsState.value = ViewState.Complete(productResponseMock)
        }

        robot.clickRecyclerViewItem(R.id.productsRecyclerView, 0)

        intended(hasComponent(ProductDetailActivity::class.java.name))
    }

    @Test
    fun whenConnectionFailed_shouldDisplayNoConnectionContainer() {
        activityTestRule.runOnUiThread {
            viewModel.enjoeiException.value = NetworkError
        }
        robot.isDisplayed(R.id.noConnectionContent)
        robot.isNotDisplayed(R.id.banner)
        robot.isNotDisplayed(R.id.productsRecyclerView)
    }
}