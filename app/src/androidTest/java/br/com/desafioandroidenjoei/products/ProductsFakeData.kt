package br.com.desafioandroidenjoei.products

import android.arch.lifecycle.MutableLiveData
import br.com.desafioandroidenjoei.base.common.exception.EnjoeiException
import br.com.desafioandroidenjoei.base.common.network.Pagination
import br.com.desafioandroidenjoei.base.data.ViewState
import br.com.desafioandroidenjoei.base.di.base
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import br.com.desafioandroidenjoei.products.model.*
import br.com.desafioandroidenjoei.products.viewmodel.ProductsViewModel
import org.mockito.Mockito


fun mockProductsViewModel(): ProductsViewModel {
    val productsState = MutableLiveData<ViewState>()
    val enjoeiException = MutableLiveData<EnjoeiException>()

    val productsViewModel = Mockito.mock(ProductsViewModel::class.java)

    val viewModelFactory = base().viewModelFactory

    Mockito.`when`(viewModelFactory.create(ProductsViewModel::class.java))
        .thenReturn(productsViewModel)


    Mockito.`when`(productsViewModel.productsState).thenReturn(productsState)
    Mockito.`when`(productsViewModel.enjoeiException).thenReturn(enjoeiException)

    return productsViewModel
}

fun mockProductDetailsViewModel(): ProductDetailsViewModel {

    val productData = MutableLiveData<Product>()
    val productDetailsViewModel = Mockito.mock(ProductDetailsViewModel::class.java)

    val viewModeFactory = base().viewModelFactory

    Mockito.`when`(viewModeFactory.create(ProductDetailsViewModel::class.java))
        .thenReturn(productDetailsViewModel)

    Mockito.`when`(productDetailsViewModel.productData).thenReturn(productData)

    return productDetailsViewModel
}

val productResponseMock = ProductsResponse().apply {
    pagination = Pagination(1, 2)
    products = kotlin.run {
        val products = mutableListOf<Product>()
        for (item in 1..3) {
            val product = Product(
                item,
                item.toDouble(),
                listOf(PhotosProduct("gravity", "crop", "publicId")),
                "Produto $item",
                item.toDouble(),
                item.toDouble(),
                "Tamanho $item",
                item,
                item,
                item,
                "Conteudo $item",
                User("User $item", item, Avatar("gravity", "crop", "publicId"))
            )
            products.add(product)
        }
        products
    }
}