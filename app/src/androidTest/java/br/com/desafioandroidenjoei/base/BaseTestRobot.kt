package br.com.desafioandroidenjoei.base

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import br.com.desafioandroidenjoei.utils.RecyclerViewMatcher
import org.hamcrest.Matchers.not

open class BaseTestRobot {

    fun clickButton(resId: Int): ViewInteraction =
        onView((withId(resId))).perform(ViewActions.click())

    fun isNotDisplayed(resId: Int): ViewInteraction =
        onView(withId(resId)).check(matches(not(isDisplayed())))

    fun isDisplayed(resId: Int): ViewInteraction =
        onView(withId(resId)).check(matches(isDisplayed()))

    fun clickRecyclerViewItem(recyclerViewId: Int, position: Int) {
        onView(withRecyclerView(recyclerViewId).atPosition(position)).perform(click())
    }

    fun withRecyclerView(recyclerViewId: Int) = RecyclerViewMatcher(recyclerViewId)

}