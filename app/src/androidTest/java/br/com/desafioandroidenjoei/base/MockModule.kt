package br.com.desafioandroidenjoei.base

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

@Module
class MockModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideContex() = application

    @Provides
    @Singleton
    fun provideViewModelFactory(): ViewModelProvider.Factory =
        Mockito.mock(ViewModelProvider.Factory::class.java)

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()
}