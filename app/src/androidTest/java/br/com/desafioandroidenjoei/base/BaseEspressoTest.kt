package br.com.desafioandroidenjoei.base

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@RunWith(AndroidJUnit4::class)
abstract class BaseEspressoTest {

    @Rule
    @JvmField
    val taskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    val app: MockEnjoeiApplication
        get() = InstrumentationRegistry
            .getInstrumentation()
            .targetContext
            .applicationContext as MockEnjoeiApplication
}