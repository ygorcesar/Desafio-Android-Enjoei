package br.com.desafioandroidenjoei.base

import br.com.desafioandroidenjoei.core.di.ApplicationComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MockModule::class])
interface MockApplicationComponent : ApplicationComponent