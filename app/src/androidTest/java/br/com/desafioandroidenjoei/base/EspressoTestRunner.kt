package br.com.desafioandroidenjoei.base

import android.app.Application
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner

class EspressoTestRunner : AndroidJUnitRunner() {

    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, MockEnjoeiApplication::class.java.name, context)
    }
}