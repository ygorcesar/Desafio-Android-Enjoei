package br.com.desafioandroidenjoei.base

import br.com.desafioandroidenjoei.core.EnjoeiApplication
import br.com.desafioandroidenjoei.core.di.ApplicationComponent

class MockEnjoeiApplication : EnjoeiApplication() {

    override fun createApplicationComponent(): ApplicationComponent {
        return DaggerMockApplicationComponent
            .builder()
            .mockModule(MockModule(this@MockEnjoeiApplication))
            .build()
    }
}