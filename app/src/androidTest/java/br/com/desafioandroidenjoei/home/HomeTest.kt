package br.com.desafioandroidenjoei.home

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.BaseEspressoTest
import br.com.desafioandroidenjoei.base.BaseTestRobot
import br.com.desafioandroidenjoei.products.mockProductsViewModel
import br.com.desafioandroidenjoei.products.viewmodel.ProductsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@LargeTest
class HomeTest : BaseEspressoTest() {

    @Rule
    @JvmField
    val activityTestRule = IntentsTestRule(HomeActivity::class.java, false, false)

    private val viewModel: ProductsViewModel = mockProductsViewModel()

    private lateinit var robot: BaseTestRobot

    @Before
    fun setup() {
        activityTestRule.launchActivity(null)

        robot = BaseTestRobot()
    }

    @Test
    fun whenActivityLaunchedShowInitialState() {
        robot.isDisplayed(R.id.viewPager)
        robot.isDisplayed(R.id.bottomNavigation)
    }

    @Test
    fun menuItemsOnNavigationIsVisible() {
        robot.isDisplayed(R.id.actionHome)
        robot.isDisplayed(R.id.actionSearch)
        robot.isDisplayed(R.id.actionCamera)
        robot.isDisplayed(R.id.actionMail)
        robot.isDisplayed(R.id.actionUser)
    }

    @Test
    fun swipeToAnotherFragmentOnPagerAdapter() {
        onView(withId(R.id.viewPager)).perform(swipeLeft())
    }

    @Test
    fun onClickHomeMenu_shouldShowProductsFragment() {
        robot.clickButton(R.id.actionSearch)
        robot.clickButton(R.id.actionHome)
        robot.isDisplayed(R.id.viewPager)
        robot.isDisplayed(R.id.bottomNavigation)
    }

    @Test
    fun onClickSearchMenu_shouldShowSearchFragment() {
        robot.clickButton(R.id.actionSearch)
        robot.isDisplayed(R.id.textTitleSearch)
    }

    @Test
    fun onClickSearchMenu_shouldShowCameraFragment() {
        robot.clickButton(R.id.actionCamera)
        robot.isDisplayed(R.id.textTitleCamera)
    }

    @Test
    fun onClickSearchMenu_shouldShowMessagesFragment() {
        robot.clickButton(R.id.actionMail)
        robot.isDisplayed(R.id.textTitleMessages)
    }

    @Test
    fun onClickSearchMenu_shouldShowProfileFragment() {
        robot.clickButton(R.id.actionUser)
        robot.isDisplayed(R.id.textTitleProfile)
    }

    @Test
    fun clikOnAllMenus_shouldShowAllNavigationsFragments(){
        robot.clickButton(R.id.actionSearch)
        robot.isDisplayed(R.id.textTitleSearch)

        robot.clickButton(R.id.actionCamera)
        robot.isDisplayed(R.id.textTitleCamera)

        robot.clickButton(R.id.actionMail)
        robot.isDisplayed(R.id.textTitleMessages)

        robot.clickButton(R.id.actionUser)
        robot.isDisplayed(R.id.textTitleProfile)

        robot.clickButton(R.id.actionHome)
        robot.isDisplayed(R.id.viewPager)
        robot.isDisplayed(R.id.bottomNavigation)
    }

}