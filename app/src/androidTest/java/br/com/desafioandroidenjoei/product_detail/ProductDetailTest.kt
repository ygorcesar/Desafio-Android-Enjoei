package br.com.desafioandroidenjoei.product_detail

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.LargeTest
import br.com.desafioandroidenjoei.R
import br.com.desafioandroidenjoei.base.BaseEspressoTest
import br.com.desafioandroidenjoei.base.BaseTestRobot
import br.com.desafioandroidenjoei.product_detail.presentation.ProductDetailActivity
import br.com.desafioandroidenjoei.product_detail.presentation.ProductDetailActivityArgs
import br.com.desafioandroidenjoei.product_detail.viewmodel.ProductDetailsViewModel
import br.com.desafioandroidenjoei.products.mockProductDetailsViewModel
import br.com.desafioandroidenjoei.products.productResponseMock
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@LargeTest
class ProductDetailTest : BaseEspressoTest() {

    @Rule
    @JvmField
    val activityTestRule = IntentsTestRule(ProductDetailActivity::class.java, false, false)

    private val viewModel: ProductDetailsViewModel = mockProductDetailsViewModel()

    private val productMock = productResponseMock.products[0]

    private lateinit var robot: BaseTestRobot

    @Before
    fun setup() {
        val intent = ProductDetailActivityArgs(productMock).intent(app)
        activityTestRule.launchActivity(intent)
        robot = BaseTestRobot()
    }


    @Test
    fun whenActivityLaunched_shouldDisplayInitialState() {
        robot.isDisplayed(R.id.productDetailViewPager)
        robot.isDisplayed(R.id.productDetailCurrency)
        robot.isDisplayed(R.id.productDetailPrice)
        robot.isDisplayed(R.id.productDetailOriginalPrice)
        robot.isDisplayed(R.id.productDetailLineLastPrice)
        robot.isDisplayed(R.id.productDetailPercentAndInfo)
        robot.isDisplayed(R.id.productDetailName)
        robot.isDisplayed(R.id.productDetailDescription)
        robot.isDisplayed(R.id.productDetailComments)
        robot.isDisplayed(R.id.productDetailCommentsCount)
        robot.isDisplayed(R.id.productDetailLike)
    }
}